'use strict';

module.exports = {
    platforms: {
        askfm: {
            key: 'askfm',
            name: 'ASKfm',
            url: 'ask.fm',
            hex: '#C6304A',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?ask\.fm\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('ask.fm/')[1].split('/')[0]
        },
        bitbucket: {
            key: 'bitbucket',
            name: 'Bitbucket',
            url: 'bitbucket.org',
            hex: '#205081',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?bitbucket\.org\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        discord: {
            key: 'discord',
            name: 'Discord',
            url: 'discord.gg',
            hex: '#7289da',
            wideThumb: false,
            urlMatch: /^(?:https?:\/\/)?(?:www\.)?(?:discord\.gg\/|discordapp\.com\/invite)(?:\S+)?$/,
            extract: (link) => (link.includes('discordapp.com/invite') ? link.split('discordapp.com/invite')[1].split('/')[1] : link.split('discord.gg/')[1].split('/')[0])
        },
        facebook: {
            key: 'facebook',
            name: 'Facebook',
            url: 'facebook.com',
            hex: '#4267b2',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?facebook\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        floatplane: {
            key: 'floatplane',
            name: 'Floatplane',
            url: 'floatplane.com',
            hex: '#08AAF2',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?floatplane\.com\/channel\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('floatplane.com/video/')[1].split('/')[0]
        },
        github: {
            key: 'github',
            name: 'Github',
            url: 'github.com',
            hex: '#24292e',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?(?:gist\.)?github\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        gitlab: {
            key: 'gitlab',
            name: 'Gitlab',
            url: 'gitlab.com',
            hex: '#E24329',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?(?:gist\.)?gitlab\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        instagram: {
            key: 'instagram',
            name: 'Instagram',
            url: 'instagram.com',
            hex: '#e33567',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?instagram\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        mixer: {
            key: 'mixer',
            name: 'Mixer',
            url: 'mixer.com',
            hex: '#29bbed',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?mixer\.com\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('mixer.com/')[1].split('/')[0]
        },
        monzo: {
            key: 'monzo',
            name: 'Monzo',
            url: 'monzo.me',
            hex: '#14233C',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?monzo\.me\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('monzo.me/')[1].split('/')[0]
        },
        myspace: {
            key: 'myspace',
            name: 'myspace',
            url: 'myspace.com',
            hex: '#231F1E',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?myspace\.com\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('myspace.com/')[1].split('/')[0]
        },
        notify: {
            key: 'notify',
            name: 'Notify',
            url: 'notify.me',
            hex: '#1bb9dc',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:blog\.)?notify\.me\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        patreon: {
            key: 'patreon',
            name: 'Patreon',
            url: 'patreon.com',
            hex: '#f96854',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?patreon\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        periscope: {
            key: 'periscope',
            name: 'Periscope',
            url: 'pscp.tv',
            hex: '#40A4C4',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?pscp\.tv\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('pscp.tv/')[1].split('/')[0]
        },
        pinterest: {
            key: 'pinterest',
            name: 'Pinterest',
            url: 'pinterest.com',
            hex: '#cb2027',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?pinterest\.com\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('pinterest.com/')[1].split('/')[0]
        },
        reddit: {
            key: 'reddit',
            name: 'Reddit',
            url: 'reddit.com',
            hex: '#4267b2',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?reddit\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        snapchat: {
            key: 'snapchat',
            name: 'snapchat',
            url: 'snapchat.com',
            hex: '#FFFC00',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?snapchat\.com\/add\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('snapchat.com/')[1].split('/')[0]
        },
        soundcloud: {
            key: 'soundcloud',
            name: 'SoundCloud',
            url: 'soundcloud.com',
            hex: '#ff8800',
            wideThumb: false,
            urlMatch: /((https:\/\/)|(http:\/\/)|(www.)|(m\.)|(\s))+(soundcloud.com\/)+[a-zA-Z0-9\-\.]+(\/)+[a-zA-Z0-9\-\.]+/,
            extract: undefined
        },
        spotify: {
            key: 'spotify',
            name: 'Spotify',
            url: 'spotify.com',
            hex: '#1DB954',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?open.spotify\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        stackoverflow: {
            key: 'stackoverflow',
            name: 'StackOverflow',
            url: 'stackoverflow.com',
            hex: '#f48024',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?stackoverflow\.com\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('stackoverflow.com/')[1].split('/')[1]
        },
        tiktok: {
            key: 'tiktok',
            name: 'TikTok',
            url: 'tiktok.com',
            hex: '#40A4C4',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?tiktok\.com\/share\/user\/([0-9]+)/,
            extract: (link) => link.split('tiktok.com/embed/')[1].split('/')[0]
        },
        twitch: {
            key: 'twitch',
            name: 'Twitch',
            url: 'twitch.tv',
            hex: '#6441A4',
            wideThumb: true,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?twitch\.tv\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('twitch.tv/')[1].split('/')[0]
        },
        twitter: {
            key: 'twitter',
            name: 'Twitter',
            url: 'twitter.com',
            hex: '#00aced',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?twitter\.com\/([a-zA-Z0-9_]+)/,
            extract: undefined
        },
        tumblr: {
            key: 'tumblr',
            name: 'Tumblr',
            url: 'tumblr.com',
            hex: '#34526f',
            wideThumb: false,
            urlMatch: /http(?:s)?:\/\/(?:www\.)?tumblr\.com\/([a-zA-Z0-9_]+)/,
            extract: (link) => link.split('tumblr.com/')[1].split('/')[1]
        },
        youtube: {
            key: 'youtube',
            name: 'YouTube',
            url: 'youtube.com',
            hex: '#ff0000',
            wideThumb: true,
            urlMatch: /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
            extract: (link) => link.match(/^.*(youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/).filter(result => result.length === 11)[0]
        },
    }
};

// to test stuff you're adding comment out the export above, run this using "node platforms.js" and console log instead
// console.log(platforms['twitch'].extract('https://twitch.tv/jamiepinelive'));
